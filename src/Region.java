import java.util.HashMap;

public class Region {

    private String name;
    private int availablePackages;
    private float unitCost;
    private int[] services;
    private HashMap<String, Integer> latencies;

    public Region(String name) {
        this.name = name;
        latencies = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvailablePackages() {
        return availablePackages;
    }

    public void setAvailablePackages(int availablePackages) {
        this.availablePackages = availablePackages;
    }

    public float getUnitCost() {
        return unitCost;
    }

    public void setUnitCost(float unitCost) {
        this.unitCost = unitCost;
    }

    public int[] getServices() {
        return services;
    }

    public void setServices(int[] services) {
        this.services = services;
    }

    public HashMap<String, Integer> getLatencies() {
        return latencies;
    }

    public void setLatencies(HashMap<String, Integer> latencies) {
        this.latencies = latencies;
    }
}
