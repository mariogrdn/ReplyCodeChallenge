import java.util.HashMap;

public class Provider {

    private String name;
    private HashMap<String, Region> regions;

    public Provider(String name) {
        regions = new HashMap<>();
        this.name = name;
    }

    public HashMap<String, Region> getRegions() {
        return regions;
    }

    public void setRegions(HashMap<String, Region> regions) {
        this.regions = regions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
