public class Project {

    private String country;
    private int penalty;
    private int[] services;

    public Project(String country, int penalty) {
        this.country = country;
        this.penalty = penalty;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public int[] getServices() {
        return services;
    }

    public void setServices(int[] services) {
        this.services = services;
    }

}
