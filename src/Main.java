import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    private static final String path = "./first_adventure.in";

    private static int V;
    private static int S;
    private static int C;
    private static int P;

    private static HashMap<String, Provider> providers = new HashMap<>();
    private static String[] regions;
    private static String[] services;

    public static void main(String[] args) {
        readData();
    }

    private static void readData() {
        try {
            Scanner s = new Scanner(new File(path));
            V = s.nextInt();
            S = s.nextInt();
            C = s.nextInt();
            P = s.nextInt();
            services = new String[S];
            //read services
            for(int c=0; c<S; c++) {
                services[c] = s.next();
            }
            regions = new String[C];
            //read regions
            for(int v=0; v<C; v++) {
                regions[v] = s.next();
            }
            for(int i = 0; i < V; i++) {
                String name = s.next();
                int R = s.nextInt();
                Provider provider = new Provider(name);
                for(int j=0; j<R; j++) {
                    String regionName = s.next();
                    int availablePackages = s.nextInt();
                    float unitCost = Float.parseFloat(s.next());
                    Region region = new Region(regionName);
                    region.setAvailablePackages(availablePackages);
                    region.setUnitCost(unitCost);
                    int[] units = new int[S];
                    for(int k=0; k<S; k++) {
                        units[k] = s.nextInt();
                    }
                    region.setServices(units);
                    HashMap<String, Integer> reg = region.getLatencies();
                    for(int l=0; l<C; l++) {
                        reg.put(regions[l], s.nextInt());
                    }
                }
                providers.put(provider.getName(), provider);
            }
            for(int i=0; i<P; i++) {
                int penalty = s.nextInt();
                String country = s.next();
                Project project = new Project(country, penalty);
                int[] units = new int[S];
                for(int j=0; j<S; j++) {
                    units[j] = s.nextInt();
                }
                project.setServices(units);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
